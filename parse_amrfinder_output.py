# Usage
# python3 parse_amrfinder_output.py <amrfinder_output_files> [OPTIONS]
# e.g python3 parse_amrfinder_output.py '~/Data/acinetobacter_baumannii/' \
#               -e '*_amrfinder.tsv' 
#               --fasta_dir ~/Data/raw_data/ghru_acinetobacter/fastas/ \
#               -g "blaOXA-23" -c "QUINOLONE" 

import csv
import sys, os
import pandas as pd
import argparse, glob
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

def is_valid_file(parser, file):
    if not os.path.isfile(file):
        parser.error('The file {} does not exist!'.format(file))
    else:
        return(file)

def check_input_amrfinder_files(parser, input_dir, input_ext, fasta_dir=None):
    input_file_list = glob.glob(f"{input_dir}/{input_ext}")
    amrfinder_files = []
    for file in input_file_list:
        valid_file = is_valid_file(parser, file)
        amrfinder_files.append(valid_file)
    if len(amrfinder_files) < 1:
        parser.error(f'No valid input file provided in {input_dir}')

    return amrfinder_files

def check_fasta_files(fasta_dir, sample_ids):
    # Check fasta files present if provided
    for sample_id in sample_ids:
        fasta_file = glob.glob(f'{fasta_dir}/{sample_id}*.fasta')
        # If file not exist
        if len(fasta_file) == 0 or not os.path.isfile(fasta_file[0]):
            sys.tracebacklimit = 0
            raise FileNotFoundError(f"Cannot find file matching {fasta_dir}/{sample_id}.fasta! Please check that the specified fasta directory and try again.")


def check_arguments(parser, args):
    # check that input amrfinder files all exist
    args.input = check_input_amrfinder_files(parser, args.input, args.input_file_ext)
    # Check that either of args.gene or args.class is specified if args.fasta_dir is specified
    if args.fasta_dir:
        if args.ext_gene == None and args.ext_class == None:
            parser.error("Fasta files have been provided but no gene or class has been specified. You must specify either a gene or class of a gene whose contig you want to extract using the -g or -c flags.")
    # Set output dir
    if not args.output_dir:
        print("Output directory not specified. Writing output files to current directory")
        args.output_dir = os.getcwd()
    elif not os.path.exists(args.output_dir):
        try:
            os.mkdir(args.output_dir)
        except OSError:
            print("Specified directory does not exist. Writing output files to current directory")
            args.output_dir = os.getcwd()
        
    return args

def parse_arguments():
    description = """
    A script to parse the output files of the NCBI amrfinder  database, summarise the results, and optionally extract the contigs carrying certain resistance genes/classes.
    To extract contigs, please provide the filepath to a directory containing the fasta files for all samples in the input amrfinder list
    """
    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('input', type=str,
        help='Path to directory containing amrfinder output file(s)')
    parser.add_argument('-o', '--output_dir', type=str, help='Output path')
    parser.add_argument('-e', '--input_file_ext', type=str, default="*_amrfinder.tsv", help='File extension for input amrfinder result files')
    parser.add_argument('-f', '--fasta_dir', type=str, help='Optional. Path to fasta files for each input file. Must specify gene contigs to extract using -g or -c')
    parser.add_argument('-g', '--ext_gene', dest='ext_gene', help='Optional. Gene symbol whose contig should be extracted. e.g. blaOXA-23')
    parser.add_argument('-c', '--ext_class', dest='ext_class', help='Optional. AMR gene subclass whose contigs should be extracted. e.g. CARBAPENEM')

    args = parser.parse_args()

    # Checks
    args = check_arguments(parser, args)

    return args

def parse_amrfinder_file(amrfinder_files_list):
    all_results = {}
    sample_ids = []
    for amrfinder_file in amrfinder_files_list:
        sample_result = {}
        with open(amrfinder_file, 'r') as f:
            # Get header names
            col_names = f.readline().split('\t')
            # get gene name index
            gene_index = col_names.index("Gene symbol")
            # print(col_names)
            # read data
            reader = csv.reader(f, delimiter='\t')
            next(reader) # skip header
            first = True
            for line in reader:
                if first == True:
                    id = line[0]  
                    sample_ids.append(id)
                # print(line)
                gene = line[gene_index]
                sample_result[gene] = dict(zip(col_names, line)) #turn header and line values to k-v pairs
                # print(gene)
                first = False
            # print(sample_result)
        all_results[id] = sample_result
    return all_results, sample_ids

def summarize_results_yes_no(all_results_dict, sample_ids):
    all_hits = dict()
    # get amr results for each sample but store as dict{gene:[list_of_samples_having_gene]}
    for sample_id in sample_ids:
        sample_hits_dict = all_results_dict[sample_id]
        for hit in sample_hits_dict: #To do: add filter for cov, ident
            if all_hits.get(hit):
                all_hits[hit].append(sample_id)
            else:
                all_hits[hit] = [sample_id]
    # print(all_hits)

    hits_sample_presence_absence_dict = dict()
    for hit, samples_list in all_hits.items():
        hits_sample_presence_absence_dict[hit] = dict()
        for sample_id in sample_ids:
            if sample_id in samples_list:
                hits_sample_presence_absence_dict[hit][sample_id] = "yes"
            else:
                hits_sample_presence_absence_dict[hit][sample_id] = "no"
    # print(hits_sample_presence_absence_dict)
    return hits_sample_presence_absence_dict

def write_summary_output(yes_no_summary_dict, output_dir):
    yes_no_summary_output_df = pd.DataFrame({key: value for key, value in yes_no_summary_dict.items()})
    yes_no_summary_output_df.index.name = "Samples"
    yes_no_summary_output_df.to_csv("{}/amrfinder_summary.csv".format(output_dir), encoding='utf-8')
    print("\nSuccess!\n\nSee summary in '{}/amrfinder_summary.csv'".format(output_dir))
    
def extract_contig_from_fasta(sample_id, fasta_dir, contig_lst, gene):
    fasta_file = glob.glob(f'{fasta_dir}/{sample_id}*.fasta')[0]
    fasta_records = []
    for seq_record in SeqIO.parse(fasta_file, "fasta"):
        if seq_record.id in contig_lst:
            # print(seq_record.id)
            fasta_records.append(
                SeqRecord(
                    seq_record.seq,
                    id = seq_record.id,
                    name = gene,
                    description = f'{sample_id} {gene}'
                )
            )
    return fasta_records

def write_out_fasta_file(fasta_records, output_dir, file_prefix):
    SeqIO.write(fasta_records, f"{output_dir}/{file_prefix}.fasta", "fasta")

# To do: make modular and DRY
def extract_determinant_contig(all_results_dict, sample_ids, fasta_dir, output_dir, ext_gene=None, ext_class=None):
    if ext_gene:
        gene_results_file = f'{output_dir}/{ext_gene}_contig_info.tsv'
        all_fasta_records = []
        with open(gene_results_file, 'w', newline='') as ext_gene_results_file:
            first = True
            for sample_id in sample_ids:
                sample_contig_lst = []
                try:
                    # print(sample_id, ext_gene, all_results_dict[sample_id][ext_gene]['Contig id'])
                    gene_results = {
                        "sample_id": sample_id, 
                        "gene": ext_gene, 
                        "subclass": all_results_dict[sample_id][ext_gene]['Subclass'], 
                        "contig": all_results_dict[sample_id][ext_gene]['Contig id'], 
                        "start": all_results_dict[sample_id][ext_gene]['Start'], 
                        "stop": all_results_dict[sample_id][ext_gene]['Stop']
                    }
                    contig_info_writer = csv.DictWriter(ext_gene_results_file, fieldnames=gene_results.keys(), delimiter = '\t')
                    if first: contig_info_writer.writeheader()
                    first = False
                    contig_info_writer.writerow(gene_results)
                    sample_contig_lst.append(gene_results["contig"])
                except KeyError:
                    pass # Sample does not have this gene  
                # print(contig_lst)
                if len(sample_contig_lst) > 0:
                    single_sample_fasta_records = extract_contig_from_fasta(sample_id, fasta_dir, sample_contig_lst, ext_gene)
                    # update fasta records for all samples
                    for fas_rec in single_sample_fasta_records: all_fasta_records.append(fas_rec)
                    # save sample-specific fastas
                    single_samples_output_dir = f'{output_dir}/sample_{ext_gene}_contigs'
                    if not os.path.exists(single_samples_output_dir): os.mkdir(single_samples_output_dir)
                    write_out_fasta_file(single_sample_fasta_records, single_samples_output_dir, f'{sample_id}_{ext_gene}_contig')
        # Save multifasta record with all extracted fasta records
        write_out_fasta_file(all_fasta_records, output_dir, f'all_{ext_gene}_contigs')
        print(f"All contigs carrying {ext_gene} written to {output_dir}")

    if ext_class:
        class_results_file = f'{output_dir}/{ext_class}_contig_info.tsv'
        with open(class_results_file, 'w', newline='') as ext_class_results_file:
            first = True
            for sample_id in sample_ids:
                try:
                    for gene, info in all_results_dict[sample_id].items():
                        # print(sample_id, all_results_dict[sample_id][gene]['Subclass'].strip().lower())
                        if all_results_dict[sample_id][gene]['Subclass'].strip().lower() == ext_class.strip().lower():
                            # print(sample_id, gene, ext_class, all_results_dict[sample_id][gene]['Contig id']) 
                            class_results = {
                                "sample_id": sample_id, 
                                "gene": gene, 
                                "subclass": all_results_dict[sample_id][gene]['Subclass'], 
                                "contig": all_results_dict[sample_id][gene]['Contig id'], 
                                "start": all_results_dict[sample_id][gene]['Start'], 
                                "stop": all_results_dict[sample_id][gene]['Stop']
                            }
                            class_writer = csv.DictWriter(ext_class_results_file, fieldnames=class_results.keys(), delimiter = '\t')
                            if first: class_writer.writeheader()
                            first = False
                            class_writer.writerow(class_results)
                except KeyError:
                    pass # Sample does not have this


def main():
    args = parse_arguments()
    all_results_dict, sample_ids = parse_amrfinder_file(args.input)
    yes_no_summary_dict = summarize_results_yes_no(all_results_dict, sample_ids)
    write_summary_output(yes_no_summary_dict, args.output_dir)

    if args.fasta_dir:
        check_fasta_files(args.fasta_dir, sample_ids)
        extract_determinant_contig(
            all_results_dict=all_results_dict, 
            sample_ids=sample_ids, 
            fasta_dir=args.fasta_dir, 
            output_dir=args.output_dir, 
            ext_gene=args.ext_gene, 
            ext_class=args.ext_class)


if __name__ == "__main__":
    main()


